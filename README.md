# BabyOS_Example

【STM32F107 使用STM32标准库函数】

------

## example/template/   

### 实验内容：

①基于MCU内部FLASH进行KV存储。 

②通过shell指令进行交互：

```shell
nr@bos:bos -v
Version:7.1.0

nr@bos:kv bos hello
bos hello 5

nr@bos:kv bos
hello
```

## example/ds18b20/   

### 实验内容：

①使用DS18B20温度传感器

```shell
nr@bos:temp -s
nr@bos:temp
r:2625

```



## example/oled/  

### 实验内容：

①使用OLED屏，通过指令显示字符串

[show] [x] [y] [string]

```shell
nr@bos:show 1 2 hello
nr@bos:show 10 10 hello

```



## example/spiflash/  

### 实验内容：

①spiflash的0地址存储启动设备的次数



## example/24c02/  

### 实验内容：

①at24lc02的0地址存储启动设备的次数





BabyOS教程更新会在公众号推送：

![](https://gitee.com/notrynohigh/BabyOS_Example/raw/master/doc/QRcode.jpg)

