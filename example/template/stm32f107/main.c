/**
 ******************************************************************************
 * @file    Project/STM32F10x_StdPeriph_Template/main.c
 * @author  MCD Application Team
 * @version V3.3.0
 * @date    04/16/2010
 * @brief   Main program body
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
 */

/* Includes ------------------------------------------------------------------*/
#include "b_os.h"
#include "board.h"

/** @addtogroup Template_Project
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

static void _CmdKVHandler(char argc, char *argv)
{
    uint8_t table[12];
    if (argc == 3)
    {
        bKV_Set(&argv[argv[1]], (uint8_t *)&argv[argv[2]], strlen(&argv[argv[2]]));
        b_log("%s %s %d\r\n", &argv[argv[1]], &argv[argv[2]], strlen(&argv[argv[2]]));
    }
    else
    {
        if(0 <= bKV_Get(&argv[argv[1]], table))
        {
            b_log("%s\r\n", table);
        }
    }
}
bSHELL_REG_INSTANCE("kv", _CmdKVHandler);

/********************************************************************************/
/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
int main()
{
    BoardInit();
    
    SysTick_Config(SystemCoreClock / _TICK_FRQ_HZ);
    NVIC_SetPriority(SysTick_IRQn, 0x0);    
    
    bInit();
    bShellInit();
    bKV_Init(MCUFLASH, 100 * 1024, 4 * 2048, 2048);
    while (1)
    {
        bExec();
    }
}

void SysTick_Handler()
{
    bHalIncSysTick();
}

void USART1_IRQHandler()
{
    uint8_t uart_dat = 0;
    if (USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
    {
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
        uart_dat = USART_ReceiveData(USART1);
        bShellParse(&uart_dat, 1);
    }
}

#ifdef USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
/**
 * @}
 */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
